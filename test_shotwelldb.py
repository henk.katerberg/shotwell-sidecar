# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.


# shotwell-sidecar database test for use with pytest

import os
import pytest
import shotwelldb as db
import shutil


@pytest.fixture
def hierarchical_data_dir(request, tmp_path):
    # The tmp_path fixture provides a temp dir unique to the test invocation
    src = os.path.join(request.config.rootpath,
                       'test-resources',
                       '4-hierarchical')
    shutil.copytree(src, tmp_path, dirs_exist_ok=True)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return tmp_path


def test_is_hierarchical_tag():
    assert not db.is_hierarchical_tag('aap'), "'aap' recognized as a hierarchical tag"
    assert db.is_hierarchical_tag('/blaat'), "'/blaat' not recognized as a hierarchical tag"
    assert db.is_hierarchical_tag('/blaat/aap'), "'/blaat/aap' not recognized as a hierarchical tag"


def test_is_flat_tag():
    assert db.is_flat_tag('aap'), "'aap' not recognized as a flat tag"
    assert not db.is_flat_tag('/blaat'), "'/blaat' recognized as a flat tag"
    assert not db.is_flat_tag('/blaat/aap'), "'/blaat/aap' recognized as a flat tag"


def test_is_hierarchical_root_tag():
    assert not db.is_hierarchical_root_tag('aap'), "'aap' recognized as a hierarchical root tag"
    assert db.is_hierarchical_root_tag('/blaat'), "'/blaat' not recognized as a hierarchical root tag"
    assert not db.is_hierarchical_root_tag('/blaat/aap'), "'/blaat/aap' recognized as a hierarchical root tag"


def test_tagtable_promote(hierarchical_data_dir):
    con = db.connect(db.dbfile(hierarchical_data_dir))
    cursor = con.cursor()
    cursor.execute('SELECT id, name FROM TagTable')
    res = cursor.fetchall()
    names = set(map(lambda t: t[1], res))
    ttids_before = set(map(lambda t: t[0], res))
    flat_tags_before = set(filter(db.is_flat_tag, names))
    assert len(flat_tags_before) == 1

    for ttid in ttids_before:
        db.tagtable_promote(con, ttid)

    cursor.execute('SELECT id, name FROM TagTable')
    res = cursor.fetchall()
    ttids_after = set(map(lambda t: t[0], res))
    flat_tags_after = set(filter(db.is_flat_tag, (map(lambda t: t[1], res))))
    assert len(flat_tags_after) == 0, f'unexpected flat tag left={after}'

    assert ttids_before == ttids_after, 'tags inserted/deleted iso renamed'
    cursor.close()
    con.close()


