# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.


# shotwell-sidecar sidecar class-level test for use with pytest

import json
import os
import pytest
import shutil
from sidecar import *


@pytest.fixture
def picture(request, tmp_path):
    src = os.path.join(request.config.rootpath,
                       'test-resources',
                       'Pictures',
                       '2023', '01', '07',
                       'IMG_20230107_193252.jpg')
    dst = os.path.join(tmp_path, 'IMG_20230107_193252.jpg')
    shutil.copyfile(src, dst)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return dst


@pytest.fixture
def non_existent_picture(tmp_path):
    return os.path.join(tmp_path, 'bogus.picture.file')


@pytest.fixture
def default_annotations():
    return {'comment':'',
            'rating':0,
            'tags':set(),
            'title':'',
            'transformations':'',
            }


@pytest.fixture
def valid_annotations():
    return {'comment':'Non-default\nmulti-line\ncomment',
            'rating':3,
            'tags':set(['First tag','Second tag']),
            'title':'Non-default title',
            'transformations':'Non-default transformation',
            }


@pytest.fixture
def valid_sidecar(request, tmp_path, picture, valid_annotations):
    valid_annotations['tags'] = list(valid_annotations['tags'])
    bn = os.path.basename(picture) + '.shotwell'
    dst = os.path.join(tmp_path, bn)
    with open(dst, 'w') as f:
        json.dump(valid_annotations, f, sort_keys=True, indent=2)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return dst


@pytest.fixture
def orphan_sidecar(request, tmp_path, non_existent_picture, valid_annotations):
    """sidecar file without picture file"""
    valid_annotations['tags'] = list(valid_annotations['tags'])
    bn = os.path.basename(non_existent_picture) + '.shotwell'
    dst = os.path.join(tmp_path, bn)
    with open(dst, 'w') as f:
        json.dump(valid_annotations, f, sort_keys=True, indent=2)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return dst


@pytest.fixture
def spurious_annotations():
    return {'comment':'Non-default\nmulti-line\ncomment',
            'rating':3,
            'title':'Non-default title',
            'transformations':'Non-default transformation',
            'spurious':'unsupported attribute',
            }


@pytest.fixture
def spurious_sidecar(request, tmp_path, picture, spurious_annotations):
    bn = os.path.basename(picture) + '.shotwell'
    dst = os.path.join(tmp_path, bn)
    with open(dst, 'w') as f:
        json.dump(spurious_annotations, f, sort_keys=True, indent=2)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return dst


@pytest.fixture
def non_existent_sidecar(tmp_path):
    return os.path.join(tmp_path, 'bogus.sidecar.file')


@pytest.fixture
def invalid_type_annotations():
    return {'comment' : 42,
            'rating': 4.2,
            'tags' : [42, 4.2],
            'title' : 42,
            'transformations' : 42,
            }


@pytest.fixture
def invalid_type_sidecar(request, tmp_path, picture, invalid_type_annotations):
    invalid_type_annotations['tags'] = list(invalid_type_annotations['tags'])
    bn = os.path.basename(picture) + '.shotwell'
    dst = os.path.join(tmp_path, bn)
    with open(dst, 'w') as f:
        json.dump(invalid_type_annotations, f, sort_keys=True, indent=2)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return dst


@pytest.fixture
def invalid_value_annotations():
    return {'rating':42,
            'tags':'First\nmulti-line\ntag',
            'title':'Non-default\nmulti-line\ncomment',
            }


def test_ctor(picture, non_existent_picture):
    with pytest.raises(TypeError):
        sc = Sidecar()
    with pytest.raises(FileNotFoundError):
        sc = Sidecar(non_existent_picture)

    sc = Sidecar(picture)
    assert sc is not None


def read_json(path):
    with open(path, 'r') as f:
        return json.load(f)


def test_factory_method(picture, valid_sidecar, orphan_sidecar, non_existent_sidecar):
    with pytest.raises(FileNotFoundError):
        sc = Sidecar.read_json(orphan_sidecar)

    with pytest.raises(FileNotFoundError):
        sc = Sidecar.read_json(non_existent_sidecar)

    with pytest.raises(Exception):
        sc = Sidecar.read_json(picture)

    sc = Sidecar.read_json(valid_sidecar)
    assert sc.photo == picture
    md = read_json(valid_sidecar)
    for k in md.keys():
        if k == 'tags':
            md[k] = set(md[k])
        assert getattr(sc, k) == md[k]


def test_factory_method_type_error(picture, invalid_type_sidecar):
    with pytest.raises(TypeError):
        sc = Sidecar.read_json(invalid_type_sidecar)


def test_factory_method_spurious_annotations(picture, spurious_sidecar):
    # read_json shall silently ignore unsupported annotations
    sc = Sidecar.read_json(spurious_sidecar)
    with pytest.raises(AttributeError):
        sc.spurious


def test_str(picture, valid_sidecar):
    sc = Sidecar.read_json(valid_sidecar)
    assert 'photo' in str(sc)
    assert 'comment' in str(sc)
    assert 'rating' in str(sc)
    assert 'tags' in str(sc)
    assert 'title' in str(sc)
    assert 'transformations' in str(sc)


def test_write_all(picture, valid_annotations):
    sc = Sidecar(picture,
                 comment = valid_annotations['comment'],
                 rating = valid_annotations['rating'],
                 tags = valid_annotations['tags'],
                 title = valid_annotations['title'],
                 transformations = valid_annotations['transformations']
                 )
    sc.write()
    assert os.path.exists(sc.file)
    j = read_json(sc.file)
    assert j['comment'] == valid_annotations['comment']
    assert j['rating'] == valid_annotations['rating']
    assert set(j['tags']) == set(valid_annotations['tags'])
    assert j['title'] == valid_annotations['title']
    assert j['transformations'] == valid_annotations['transformations']


def test_write_skip_defaults(picture, default_annotations, valid_annotations):
    sc = Sidecar(picture)
    sc.write()
    assert not os.path.exists(sc.file), 'wrote sidecar file while no user-defined metadata'

    sc = Sidecar(picture,
                 comment = default_annotations['comment'],
                 rating = default_annotations['rating'],
                 tags = default_annotations['tags'],
                 title = default_annotations['title'],
                 transformations = default_annotations['transformations'])
    sc.write()
    assert not os.path.exists(sc.file), 'wrote sidecar file while no user-defined metadata'

    sc = Sidecar(picture,
                 #comment = valid_annotations['comment'],
                 rating = valid_annotations['rating'],
                 #tags = valid_annotations['tags'],
                 title = valid_annotations['title'],
                 #transformations = valid_annotations['transformations']
                 )
    sc.write()
    assert os.path.exists(sc.file)
    j = read_json(sc.file)
    assert 'comment' not in j.keys()
    assert j['rating'] == valid_annotations['rating']
    assert 'tags' not in j.keys()
    assert j['title'] == valid_annotations['title']
    assert 'transformations' not in j.keys()

    sc = Sidecar(picture,
                 comment = valid_annotations['comment'],
                 #rating = valid_annotations['rating'],
                 tags = valid_annotations['tags'],
                 #title = valid_annotations['title'],
                 transformations = valid_annotations['transformations']
                 )
    sc.write()
    assert os.path.exists(sc.file)
    j = read_json(sc.file)
    assert j['comment'] == valid_annotations['comment']
    assert 'rating' not in j.keys()
    assert set(j['tags']) == set(valid_annotations['tags'])
    assert 'title' not in j.keys()
    assert j['transformations'] == valid_annotations['transformations']


def test_get(picture, valid_annotations):
    sc = Sidecar(picture,
                 comment = valid_annotations['comment'],
                 rating = valid_annotations['rating'],
                 tags = valid_annotations['tags'],
                 title = valid_annotations['title'],
                 transformations = valid_annotations['transformations']
                 )

    assert sc.comment == valid_annotations['comment']
    assert sc.rating == valid_annotations['rating']
    assert sc.tags == set(valid_annotations['tags'])
    assert sc.title == valid_annotations['title']
    assert sc.transformations == valid_annotations['transformations']


def test_null_values(picture, default_annotations):
    sc = Sidecar(picture)

    assert sc.comment is None
    assert sc.rating is None
    assert sc.tags is None
    assert sc.title is None
    assert sc.transformations is None


def test_set(picture, valid_annotations):
    sc = Sidecar(picture)
    sc.comment = valid_annotations['comment']
    sc.rating = valid_annotations['rating']
    sc.tags = valid_annotations['tags']
    sc.title = valid_annotations['title']
    sc.transformations = valid_annotations['transformations']

    assert sc.photo == picture
    assert sc.comment == valid_annotations['comment']
    assert sc.rating == valid_annotations['rating']
    assert sc.tags == set(valid_annotations['tags'])
    assert sc.title == valid_annotations['title']
    assert sc.transformations == valid_annotations['transformations']


def test_set_defaults(picture, default_annotations, valid_annotations):
    sc = Sidecar(picture,
                 comment = valid_annotations['comment'],
                 rating = valid_annotations['rating'],
                 tags = valid_annotations['tags'],
                 title = valid_annotations['title'],
                 transformations = valid_annotations['transformations']
                 )
    sc.comment = default_annotations['comment']
    sc.rating = default_annotations['rating']
    sc.tags = default_annotations['tags']
    sc.title = default_annotations['title']
    sc.transformations = default_annotations['transformations']
    
    assert sc.comment is None
    assert sc.rating is None
    assert sc.tags is None
    assert sc.title is None
    assert sc.transformations is None


def test_del(picture, valid_annotations):
    sc = Sidecar(picture,
                 comment = valid_annotations['comment'],
                 rating = valid_annotations['rating'],
                 tags = valid_annotations['tags'],
                 title = valid_annotations['title'],
                 transformations = valid_annotations['transformations']
                 )
    del sc.comment
    del sc.rating
    del sc.tags
    del sc.title
    del sc.transformations
    
    assert sc.comment is None
    assert sc.rating is None
    assert sc.tags is None
    assert sc.title is None
    assert sc.transformations is None


def test_types(picture, invalid_type_annotations):
    sc = Sidecar(picture)
    
    with pytest.raises(TypeError):
        sc.comment = invalid_type_annotations['comment']
    with pytest.raises(TypeError):
        sc.rating = invalid_type_annotations['rating']
    with pytest.raises(TypeError):
        sc.tags = invalid_type_annotations['tags']
    with pytest.raises(TypeError):
        sc.title = invalid_type_annotations['title']
    with pytest.raises(TypeError):
        sc.transformations = invalid_type_annotations['transformations']


def test_values(picture, invalid_value_annotations):
    sc = Sidecar(picture)

    # comment: any string value allowed
    with pytest.raises(ValueError):
        sc.rating = invalid_value_annotations['rating']
    with pytest.raises(ValueError):
        sc.tags = invalid_value_annotations['tags']
    with pytest.raises(ValueError):
        sc.title = invalid_value_annotations['title']
    # transformations: any string value allowed
