# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

import json
import os
import re


class Sidecar:
    """Photo meta-data in sidecar file"""
    default = {'comment':'',
               'rating':0,
               'tags':set(),
               'title':'',
               'transformations':'',
               }

    def __init__(self,
                 photo,
                 comment=None,
                 rating=None,
                 tags=None,
                 title = None,
                 transformations=None):
        self.photo = photo
        self.comment = comment
        self.rating = rating
        self.tags = tags
        self.title = title
        self.transformations = transformations


    def __str__(self):
        metadata = {}
        metadata['photo'] = self.photo
        if self.comment is not None:
            metadata['comment'] = self.comment
        if self.rating is not None:
            metadata['rating'] = self.rating
        if self.tags is not None:
            # json does not support sets => transform to list
            metadata['tags'] = list(self.tags)
        if self.title is not None:
            metadata['title'] = self.title
        if self.transformations is not None:
            metadata['transformations'] = self.transformations
        return str(metadata)


    @classmethod
    def read_json(cls, file):
        """Read Sidecar from json file"""
        try:
            if not os.path.exists(file):
                raise FileNotFoundError(f'Sidecar file not found: {file}')
            sc = Sidecar(re.sub(r'\.shotwell', '', file))
            with open(file, 'r') as f:
                md = json.load(f)
            for k in Sidecar.default.keys():
                if k in md.keys():
                    setattr(sc, k, md[k])
            return sc
        except ValueError as e:
            raise Exception('Invalid json file content')


    # photo is a required property, cannot be deleted, no sidecar file
    # can exist if associated photo file does not exist
    @property
    def photo(self):
        """Absolute path to associated photo file"""
        return self._photo

    @photo.setter
    def photo(self, path):
        if not os.path.exists(path):
            raise FileNotFoundError(f'Photo file not found: {path}')
        self._photo = os.path.abspath(path)


    # Optional properties shall only hold a non-null value if it is
    # not the default value used in the Shotwell database.
    @property
    def comment(self):
        """Shotwell comment string for photo"""
        return self._comment

    @comment.setter
    def comment(self, value):
        if not (value is None or
                isinstance(value, str)):
            raise TypeError(f'Invalid comment type: {type(value)}')
        if (value is None or
            value == self.default['comment']):
            self._comment = None
        else:
            self._comment = value

    @comment.deleter
    def comment(self):
        self._comment = None


    @property
    def rating(self):
        """Shotwell star rating for photo"""
        return self._rating

    @rating.setter
    def rating(self, value):
        if not (value is None or
                isinstance(value, int)):
            raise TypeError(f'Invalid rating type: {type(value)}')
        if not (value is None or
                (0 <= value and value <=5)):
            raise ValueError(f'Invalid rating value: {value}')
        if (value is None or
             value == self.default['rating']):
            self._rating = None
        else:
            self._rating = value

    @rating.deleter
    def rating(self):
        self._rating = None


    # Tag list internally represented as set to facilitate equivalence
    # testing by == operator
    @property
    def tags(self):
        """Shotwell tag name list for photo"""
        return self._tags

    @tags.setter
    def tags(self, value):
        if not (value is None or
                (isinstance(value, (list, set)) and
                 all(map(lambda x: isinstance(x, str), value))) or
                isinstance(value, str)):
            raise TypeError('Invalid tags type: {type(value)"')
        if isinstance(value, str):
            value = [value]
        if isinstance(value, list):
            value = set(value)
        if (value is None or
            value == self.default['tags']):
            self._tags = None
        else:
            for tag in value:
                if '\n' in tag:
                    raise ValueError('Multi-line tag names not allowed')
            self._tags = set(value)

    @tags.deleter
    def tags(self):
        self._tags = None


    @property
    def title(self):
        """Shotwell title string for photo"""
        return self._title

    @title.setter
    def title(self, value):
        if not (value is None or
                isinstance(value, str)):
            raise TypeError(f'Invalid title type: {type(value)}')
        if (value is None or
            value == self.default['title']):
            self._title = None
        else:
            if '\n' in value:
                raise ValueError(f'Multi-line title strings not allowed')
            self._title = value

    @title.deleter
    def title(self):
        self._title = None


    @property
    def transformations(self):
        """Shotwell transformations blob string for photo"""
        return self._transformations

    @transformations.setter
    def transformations(self, value):
        if not (value is None or
                isinstance(value, str)):
            raise TypeError(f'Invalid transformations type: {type(value)}')
        if (value is None or
            value == self.default['transformations']):
            self._transformations = None
        else:
            self._transformations = value

    @transformations.deleter
    def transformations(self):
        self._transformations = None


    @property
    def file(self):
        """Absolute path to sidecar file"""
        return self._photo + '.shotwell'

    def write(self):
        """
        Write metadata to json file

        Returns
        -------
        bool
            True if file written, False if file write skipped
        """
        metadata = {}
        if self.comment is not None:
            metadata['comment'] = self.comment
        if self.rating is not None:
            metadata['rating'] = self.rating
        if self.tags is not None:
            # json does not support sets => transform to list
            metadata['tags'] = list(self.tags)
        if self.title is not None:
            metadata['title'] = self.title
        if self.transformations is not None:
            metadata['transformations'] = self.transformations

        if metadata != {}:
            with open(self.file, 'w') as f:
                f.write(json.dumps(metadata, sort_keys=True, indent=2))
            return True
        return False
