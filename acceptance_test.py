# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.


# shotwell-sidecar acceptance test for use with pytest

import json
import os
import pytest
import re
import shutil
import subprocess

@pytest.fixture
def pictures_dir(request, tmp_path):
    # test database refers to pictures in '/tmp/shotwell-sidecar-test-lib'
    src = os.path.join(request.config.rootpath, 'test-resources', 'Pictures')
    dst = os.path.join(tmp_path, 'Pictures')
    lnk = os.path.join('/tmp', 'shotwell-sidecar-test-lib')

    # copy picture files
    shutil.copytree(src,dst)
    if not os.path.exists(lnk):
        os.symlink(dst, lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink already exists')

    yield lnk

    # only rm symlink, tmp_path removes old temp dir after 3 sessions
    if os.path.exists(lnk):
        os.unlink(lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink not found')


@pytest.fixture
def pictures(pictures_dir):
    files = [os.path.join(root, name)
             for root, dirs, files in os.walk(pictures_dir)
             for name in files
             if name.endswith((".jpg", ".jpeg"))]
    return files


@pytest.fixture
def annotated_pictures_dir(request, tmp_path, expected_flat_sidecar_dir):
    # Pictures directory containing photo files and related sidecar files
    # as if metadata-export was run with the source Shotwell database

    src = os.path.join(request.config.rootpath, 'test-resources', 'Pictures')
    dst = os.path.join(tmp_path, 'Pictures')
    # test database refers to pictures in '/tmp/shotwell-sidecar-test-lib'
    lnk = os.path.join('/tmp', 'shotwell-sidecar-test-lib')

    # copy picture files
    shutil.copytree(src, dst)
    # copy sidecar files
    picture_files = [(branch, leaf)
                     for branch, dirs, files in os.walk(dst)
                     for leaf in files
                     if leaf.endswith((".jpg", ".jpeg"))]
    for (branch, leaf) in picture_files:
        sidecar = leaf + '.shotwell'
        src = os.path.join(expected_flat_sidecar_dir, sidecar)
        if os.path.exists(src):
            shutil.copy(src, branch)

    if not os.path.exists(lnk):
        os.symlink(dst, lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink already exists')

    yield lnk

    # only rm symlink, tmp_path removes old temp dir after 3 sessions
    if os.path.exists(lnk):
        os.unlink(lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink not found')


@pytest.fixture
def annotated_pictures(annotated_pictures_dir):
    """List of photo files managed by Shotwell"""
    files = [os.path.join(root, name)
             for root, dirs, files in os.walk(annotated_pictures_dir)
             for name in files
             if name.endswith((".jpg", ".jpeg"))]
    return files


@pytest.fixture
def annotated_sidecars(annotated_pictures_dir):
    """List of sidecar files for photo files managed by Shotwell"""
    sidecars = [os.path.join(branch, leaf)
                for (branch, dirs, leaves) in os.walk(annotated_pictures_dir)
                for leaf in leaves
                if leaf.endswith('.shotwell')]
    return sidecars


@pytest.fixture
def hierarchical_pictures_dir(request, tmp_path, expected_hierarchical_sidecar_dir):
    # Pictures directory containing photo files and related sidecar files
    # as if metadata-export was run with the source Shotwell database

    src = os.path.join(request.config.rootpath, 'test-resources', 'Pictures')
    dst = os.path.join(tmp_path, 'Pictures')
    # test database refers to pictures in '/tmp/shotwell-sidecar-test-lib'
    lnk = os.path.join('/tmp', 'shotwell-sidecar-test-lib')

    # copy picture files
    shutil.copytree(src, dst)
    # copy sidecar files
    picture_files = [(branch, leaf)
                     for branch, dirs, files in os.walk(dst)
                     for leaf in files
                     if leaf.endswith((".jpg", ".jpeg"))]
    for (branch, leaf) in picture_files:
        sidecar = leaf + '.shotwell'
        src = os.path.join(expected_hierarchical_sidecar_dir, sidecar)
        if os.path.exists(src):
            shutil.copy(src, branch)

    if not os.path.exists(lnk):
        os.symlink(dst, lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink already exists')

    yield lnk

    # only rm symlink, tmp_path removes old temp dir after 3 sessions
    if os.path.exists(lnk):
        os.unlink(lnk)
    else: # pragma: no cover
        raise Exception('test pictures symlink not found')


@pytest.fixture
def hierarchical_pictures(hierarchical_pictures_dir):
    """List of photo files managed by Shotwell"""
    files = [os.path.join(root, name)
             for root, dirs, files in os.walk(hierarchical_pictures_dir)
             for name in files
             if name.endswith((".jpg", ".jpeg"))]
    return files


@pytest.fixture
def hierarchical_sidecars(hierarchical_pictures_dir):
    """List of sidecar files for photo files managed by Shotwell"""
    sidecars = [os.path.join(branch, leaf)
                for (branch, dirs, leaves) in os.walk(hierarchical_pictures_dir)
                for leaf in leaves
                if leaf.endswith('.shotwell')]
    return sidecars


@pytest.fixture
def imported_data_dir(request, tmp_path):
    # Shotwell data dir (data/thumbs) after initial Showtell image import
    # i.e. no user annotations present in database
    src = os.path.join(request.config.rootpath,
                       'test-resources',
                       '2-imported')
    # The tmp_path fixture provides a temp dir unique to the test invocation
    shutil.copytree(src, tmp_path, dirs_exist_ok=True)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return tmp_path


@pytest.fixture
def annotated_data_dir(request, tmp_path):
    # The tmp_path fixture provides a temp dir unique to the test invocation
    src = os.path.join(request.config.rootpath,
                       'test-resources',
                       '3-annotated')
    shutil.copytree(src, tmp_path, dirs_exist_ok=True)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return tmp_path


@pytest.fixture
def hierarchical_data_dir(request, tmp_path):
    # The tmp_path fixture provides a temp dir unique to the test invocation
    src = os.path.join(request.config.rootpath,
                       'test-resources',
                       '4-hierarchical')
    shutil.copytree(src, tmp_path, dirs_exist_ok=True)

    # return iso yield: tmp_path removes old temp dir after 3 sessions
    return tmp_path


@pytest.fixture
def expected_flat_sidecar_dir(request, tmp_path):
    src = os.path.join(request.config.rootpath, 'test-resources', '3-annotated', 'expected')
    return src


@pytest.fixture
def expected_hierarchical_sidecar_dir(request, tmp_path):
    src = os.path.join(request.config.rootpath, 'test-resources', '4-hierarchical', 'expected')
    return src


def test_help():
    """ -h and --help shall present a manual page """
    print('test_help')
    cp_smeh = subprocess.run(['./shotwell-metadata-export',
                              '-h'],
                             capture_output=True,
                             text=True)
    assert cp_smeh.returncode == 0
    assert re.match('^usage: shotwell-metadata-export.*',
                    cp_smeh.stdout)

    cp_smehelp = subprocess.run(['./shotwell-metadata-export',
                                 '--help'],
                                capture_output=True,
                                text=True)
    assert cp_smehelp.returncode == 0
    assert re.match('^usage: shotwell-metadata-export.*',
                    cp_smehelp.stdout)

    cp_smih = subprocess.run(['./shotwell-metadata-import',
                              '-h'],
                             capture_output=True,
                             text=True)
    assert cp_smih.returncode == 0
    assert re.match('^usage: shotwell-metadata-import.*',
                    cp_smih.stdout)

    cp_smihelp = subprocess.run(['./shotwell-metadata-import',
                                 '--help'],
                                capture_output=True,
                                text=True)
    assert cp_smihelp.returncode == 0
    assert re.match('^usage: shotwell-metadata-import.*',
                    cp_smihelp.stdout)


def assert_equivalent_sidecars(file_a, file_b):
    with open(file_a, 'r') as f:
        a = json.load(f)
    with open(file_b, 'r') as f:
        b = json.load(f)
    # convert lists to sets before compare
    for d in [a,b]:
        for k in d.keys():
            if type(d[k]) == type([]):
                d[k] = set(d[k])
    assert a == b, f'sidecar files {file_a} and {file_b} not equivalent: {a} != {b}'


# Work around pytest.mark.parametrize inability to parametrize test
# with differing fixtures: use a helper function.
def verify_export(data_dir, pictures, expected_sidecar_dir):
    # export metadata for all images in database
    # == export metadata for all jpg files in <pictures>
    for file in pictures:
        cp = subprocess.run(['./shotwell-metadata-export',
                             '-d',
                             f'{data_dir}',
                             file],
                             capture_output=True,
                             text=True)
        assert cp.returncode == 0, f'metadata-export failed for {file}'

        # is sidecar file present/absent as expected?
        expected_sidecar_file = os.path.join(expected_sidecar_dir,
                                             os.path.basename(file) + '.shotwell')
        expected_sidecar = os.path.exists(expected_sidecar_file)
        exported_sidecar_file = file + '.shotwell'
        exported_sidecar = os.path.exists(exported_sidecar_file)
        if expected_sidecar != exported_sidecar: # pragma: no cover
            # only executed when test fails => exclude from coverage measure
            if expected_sidecar:
                msg = f'expected sidecar absent: {expected_sidecar_file}'
            else:
                msg = f'unexpected sidecar present: {exported_sidecar_file}'
        assert expected_sidecar == exported_sidecar, msg

        if expected_sidecar:
            assert_equivalent_sidecars(expected_sidecar_file, exported_sidecar_file)


def test_export_flat(annotated_data_dir, pictures, expected_flat_sidecar_dir):
    """Test export from photo.db containing only flat tags"""
    verify_export(annotated_data_dir, pictures, expected_flat_sidecar_dir)


def test_export_hierarchical(hierarchical_data_dir, pictures, expected_hierarchical_sidecar_dir):
    """Test export from photo.db containing some hierarchical tags and some flat tags"""
    verify_export(hierarchical_data_dir, pictures, expected_hierarchical_sidecar_dir)


# Work around pytest.mark.parametrize inability to parametrize test
# with differing fixtures: use a helper function.
def verify_import(data_dir,
                  pictures,
                  sidecars,
                  expected_sidecar_dir):
    # metadata-import for each sidecar file in <pictures>
    for sidecar in sidecars:
        cp = subprocess.run(['./shotwell-metadata-import',
                             '-d',
                             data_dir,
                             sidecar],
                            capture_output = True,
                            text = True)
        assert cp.returncode == 0, f'metadata-import failed for {sidecar}'

    # remove sidecar files from <pictures>
    for sidecar in sidecars:
        os.unlink(sidecar)

    # export metadata and require sidecars equivalent with expected sidecars 
    verify_export(data_dir, pictures, expected_sidecar_dir)


def test_import_flat(imported_data_dir,
                     annotated_pictures,
                     annotated_sidecars,
                     expected_flat_sidecar_dir):
    verify_import(imported_data_dir,
                  annotated_pictures,
                  annotated_sidecars,
                  expected_flat_sidecar_dir)


def test_import_hierarchical(annotated_data_dir,
                             hierarchical_pictures,
                             hierarchical_sidecars,
                             expected_hierarchical_sidecar_dir):
    verify_import(annotated_data_dir,
                  hierarchical_pictures,
                  hierarchical_sidecars,
                  expected_hierarchical_sidecar_dir)


def test_import_hierarchical_tag_promotion(annotated_data_dir,
                                           pictures_dir,
                                           expected_hierarchical_sidecar_dir):
    # Test test_import is insufficient to validate hierarchical tag
    # import. Shotwell promotes a flat tag (no leading '/' in tag
    # name) to a hierarchical root tag (leading '/' in tag name) when
    # a child tag is added to it. I.e. after a tag import that led to
    # tag promotion all photos tagged with the flat tag shall now be
    # tagged with the hierarchical root tag.

    # Additional test required: Verify tag promotion.
    # Start with db containing annotations with flat tags:
    #    ['Crochetted', 'Duckie', 'Red', 'Rubber']
    # Import one sidecar with hierarchical root tag(s):
    #    ['/Duckie', 'Red', '/Duckie/Rubber']
    src = os.path.join(expected_hierarchical_sidecar_dir,
                       'IMG_20230107_193252.jpg.shotwell')
    htag_sidecar_file = os.path.join(pictures_dir,
                                     '2023','01','07',
                                     'IMG_20230107_193252.jpg.shotwell')
    assert os.path.exists(src)
    if os.path.exists(src):
        shutil.copy(src, htag_sidecar_file)

    cp = subprocess.run(['./shotwell-metadata-import',
                         '-d',
                         f'{annotated_data_dir}',
                         htag_sidecar_file],
                         capture_output=True,
                         text=True)
    assert cp.returncode == 0 # import process success?

    # Verify that flat tag 'Duckie' replaced by equally named root tag '/Duckie
    # Verify that new tag '/Duckie/Rubber' added
    # Verify that flat tag 'Rubber' still exists as flat tag in db
    ftag_photo_file = os.path.join(pictures_dir,
                                   '2023','01','07',
                                   'IMG_20230107_193252.jpg') # {'Rubber','Duckie', 'Red'}
    cp = subprocess.run(['./shotwell-metadata-export',
                         '-d',
                         f'{annotated_data_dir}',
                         ftag_photo_file],
                         capture_output=True,
                         text=True)
    assert cp.returncode == 0 # export process success?

    ftag_sidecar_file = os.path.join(pictures_dir,
                                     '2023','01','07',
                                     'IMG_20230107_193252.jpg.shotwell') # {'/Duckie/Rubber','/Duckie'}
    soll_tags = set(['/Duckie/Rubber','/Duckie', 'Red'])
    with open(ftag_sidecar_file, 'r') as f:
        a = json.load(f)
    ist_tags = set(a['tags'])
    assert ist_tags == soll_tags, f'Tag promotion failed {ist_tags} != {soll_tags}'

    # Verify that image referring to flat tags 'Duckie' and 'Rubber'
    # refers to tags '/Duckie' and 'Rubber'
    ftag_photo_file = os.path.join(pictures_dir,
                                   '2023','01','07',
                                   'IMG_20230107_193323.jpg') # {'Rubber','Duckie'}
    cp = subprocess.run(['./shotwell-metadata-export',
                         '-d',
                         f'{annotated_data_dir}',
                         ftag_photo_file],
                         capture_output=True,
                         text=True)
    assert cp.returncode == 0 # export process success?

    ftag_sidecar_file = os.path.join(pictures_dir,
                                     '2023','01','07',
                                     'IMG_20230107_193323.jpg.shotwell') # {'Rubber','/Duckie'}
    soll_tags = set(['Rubber','/Duckie'])
    with open(ftag_sidecar_file, 'r') as f:
        a = json.load(f)
    ist_tags = set(a['tags'])
    assert ist_tags == soll_tags, f'Tag promotion failed {ist_tags} != {soll_tags}'


def test_invalidate_thumb(imported_data_dir, annotated_pictures_dir):
    # - metadata-import shall invalidate cached thumb-nail image if
    #   sidecar file contains tranformations
    # - metadata-import shall not alter cached thumb-nail image if
    #   sidecar file does not contain transformations
    def sidecar_transforms(file):
        with open(file, 'r') as f:
            return 'transformations' in json.loads(f.read())

    small_thumbs = os.path.join(imported_data_dir, 'thumbs', 'thumbs128')
    large_thumbs = os.path.join(imported_data_dir, 'thumbs', 'thumbs360')

    photo_table_ids = {'IMG_20230107_193234.jpg' : 1,
                       'IMG_20230107_193252.jpg' : 2,
                       'IMG_20230107_193323.jpg' : 3,
                       'IMG_20230107_193345.jpg' : 4,
                       'IMG_20230107_193417.jpg' : 5,
                       'IMG_20230107_193449.jpg' : 6,
                       'IMG_20230107_193506.jpg' : 7,
                       }
    thumbs = dict(map(lambda item: (os.path.join(annotated_pictures_dir,
                                                 '2023', '01','07',
                                                 item[0]+'.shotwell'),
                                    {'small' : os.path.join(small_thumbs,
                                                            f'thumb{item[1]:016x}.jpg'),
                                     'large' : os.path.join(large_thumbs,
                                                            f'thumb{item[1]:016x}.jpg')}
                                    ), photo_table_ids.items()))
    for s, t in thumbs.items():
        if os.path.exists(s):
            cp = subprocess.run(['./shotwell-metadata-import',
                                 '-d',
                                 f'{imported_data_dir}',
                                 s],
                                capture_output=True,
                                text=True)
            assert cp.returncode == 0, cp.stderr

            if sidecar_transforms(s):
                msg = f'thumb-nail not erased'
                assert not os.path.exists(t['small']), msg
                assert not os.path.exists(t['large']), msg
            else:
                msg = f'thumb-nail erased'
                assert os.path.exists(t['small']), msg
                assert os.path.exists(t['large']), msg
