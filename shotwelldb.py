# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

"""
Shotwell database utility functions
"""
import math
import os
import re
import sqlite3
import time


# Shotwell keeps its database in file <datadir>/data/photo.db
default_datadir = '~/.local/share/shotwell/'
default_dbfile = 'data/photo.db' # relative to datadir

# Shotwell caches thumb-nail images in <datadir>/thumbs/thumbs128 and
# <datadir>/thumbs/thumbs360 if command-line option '-d' was
# used. Otherwise, ~/.cache/shotwell/thumbs/ is used.
default_thumbsdir = '~/.cache/shotwell/thumbs/'

def dbfile(datadir):
    """
    Absolute path to Shotwell sqlite3 database file.

    Parameters
    ----------
    datadir : str
        Shotwell -d/--datadir = path to dir with /data and /thumbs subdirs
    """
    return os.path.abspath(os.path.expanduser(datadir)) + '/' + default_dbfile


def thumbsdir(datadir):
    """
    Absolute path to Shotwell thumbnail cache base directory.

    Parameters
    ----------
    datadir : str
        Shotwell -d/--datadir = path to dir with /data and /thumbs subdirs
    """
    if (os.path.abspath(os.path.expanduser(datadir)) ==
        os.path.abspath(os.path.expanduser(default_datadir))):
        return os.path.abspath(os.path.expanduser(default_thumbsdir))
    else:
        return os.path.abspath(os.path.expanduser(datadir)) + '/thumbs'


def isSqlite3Db(dbpath):
    """
    Check if file is a valid SQlite3 database

    https://www.sqlite.org/fileformat.html

    Parameters
    ----------
    dbpath : str

    Returns
    -------
    bool
    """
    if not os.path.isfile(dbpath): return False
    sz = os.path.getsize(dbpath)

    # New sqlite3 files created in recent libraries are empty! => valid
    if sz == 0: return True

    # SQLite database file header is 100 bytes
    if sz < 100: return False
    
    # Validate file header
    with open(dbpath, 'rb') as fd: header = fd.read(100)    

    return (header[:16] == b'SQLite format 3\x00')

def isShotwellDb(dbpath):
    """
    Check if file is a valid Shotwell database
    
    For shotwell-util the availability of required database tables
    'PhotoTable', 'TagTable' and 'VersionTable' is sufficient proof
    that dbpath is a Shotwell database file.
    
    Parameters
    ----------
    dbpath : str
        path to sqlite3 database file
        
    Returns
    -------
    bool

    """
    con = sqlite3.connect(dbpath)
    cursor = con.cursor()
    cursor.execute('SELECT name FROM sqlite_master WHERE type="table"')
    rslt = cursor.fetchall()
    cursor.close()
    con.close()
    
    tables = set(map(lambda x: x[0], rslt))
    required = {'PhotoTable','TagTable','VersionTable'}
    return required.issubset(tables)


def isSupportedDb(dbpath):
    """
    Check if Shotwell db schema and app version match test set.
    
    Parameters
    ----------
    dbpath : str
        path to sqlite3 database file
        
    Returns
    -------
    bool
    """
    tested_versions = [{'schema':20, 'app':'0.22.0'}, # Ubuntu 20.04.5 LTS, Shotwell 0.30.10
                       {'schema':20, 'app':'0.30.11'}, # Debian 11 (bullseye)
                      ]
    con = sqlite3.connect(dbpath)
    cursor = con.cursor()
    cursor.execute('SELECT schema_version, app_version FROM VersionTable')
    res = cursor.fetchall()
    cursor.close()
    con.close()

    supported = False
    for schema, app in res:
        for v in tested_versions:
            supported = supported or (schema == v['schema'] and app == v['app'])

    return supported

def connect(dbpath, force=False):
    """
    Connect to Shotwell database
    
    Parameters
    ----------
    dbpath : str
        Path to Shotwell sqlite3 database file
    force : bool
        Override Shotwell db version check

    Returns
    -------
    sqlite3.Connection

    Raises
    ------
    Exception
        When connecting impossible or unreasonable
    """
    if not os.path.exists(dbpath):
        raise Exception(f'Database file {dbpath} not found')
    if not isSqlite3Db(dbpath):
        raise Exception(f'Not an Sqlite3 database: {dbpath}')
    if not isShotwellDb(dbpath):
        raise Exception(f'Not a Shotwell database: {dbpath}')
    if not force and not isSupportedDb(dbpath):
        raise Exception(f'Unsupported Shotwell database version')
    return sqlite3.connect(dbpath)


def phototable_id(con, path):
    """
    Shotwell PhotoTable.id for photo file <path>.
    
    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to a photo file

    Returns
    -------
    int
        PhotoTable.id for photo file in <path>
    
    Raises
    ------
    Exception
        When relative path used for photo file
    """
    if not os.path.isabs(path):
        raise Exception(f'Shotwell db requires absolute path for photo file')

    cursor = con.cursor()
    cursor.execute('SELECT id FROM PhotoTable WHERE filename=?', [path])
    res = cursor.fetchall() # returns tuple of tuples
    cursor.close()
    # shotwell path is an absolute path => at most one entry in shotwell db
    if len(res) > 0:
        return res[0][0]
    else:
        return None


def phototable_title(con, path):
    """
    Get Shotwell title for photo file

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to photo file related to title
    """
    cursor = con.cursor()
    cursor.execute('SELECT title FROM PhotoTable WHERE filename=?', [path])
    res = cursor.fetchone() # returns a single tuple
    cursor.close()
    if res is not None:
        return res[0]


def phototable_title_photo(con, ptid, title):
    """
    Title photo with PhotoTable.id <ptid>

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ptid : int
        PhotoTable.id of photo file to annotate
    title : str
        Shotwell title
    """
    cursor = con.cursor()
    cursor.execute('UPDATE PhotoTable SET title = ? WHERE id = ?',
                   [title, ptid])
    con.commit()
    cursor.close()
    return


def phototable_comment(con, path):
    """
    Get Shotwell comment for photo file

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to photo file related to comment
    """
    cursor = con.cursor()
    cursor.execute('SELECT comment FROM PhotoTable WHERE filename=?', [path])
    res = cursor.fetchone() # returns a single tuple
    cursor.close()
    if res is not None:
        comment = res[0]
        # Shotwell 0.30.11 writes phantom comment to db when
        # no user-supplied comment available.
        if (comment == 'charset=Ascii binary comment' or
            comment == 'charset=Ascii'):
            comment = None
        return comment


def phototable_comment_photo(con, ptid, comment):
    """
    Comment photo with PhotoTable.id <ptid>

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ptid : int
        PhotoTable.id of photo file to comment
    comment : str
        Shotwell comment
    """
    cursor = con.cursor()
    cursor.execute('UPDATE PhotoTable SET comment = ? WHERE id = ?',
                   [comment, ptid])
    con.commit()
    cursor.close()
    return


def phototable_rating(con, path):
    """
    Get Shotwell star rating for photo file

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to photo file related to rating
    """
    cursor = con.cursor()
    cursor.execute('SELECT rating FROM PhotoTable WHERE filename=?', [path])
    res = cursor.fetchone() # returns a single tuple
    cursor.close()
    if res is not None:
        return res[0]


def phototable_rate_photo(con, ptid, rating):
    """
    Transform photo with PhotoTable.id <ptid>

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ptid : int
        PhotoTable.id of photo file to transform
    rating : int
        Shotwell rating
    """
    cursor = con.cursor()
    cursor.execute('UPDATE PhotoTable SET rating = ? WHERE id = ?',
                   [rating, ptid])
    con.commit()
    cursor.close()
    return


def phototable_transformations(con, path):
    """
    Get Shotwell transformations for photo file

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to photo file related to transformations
    """
    cursor = con.cursor()
    cursor.execute('SELECT transformations FROM PhotoTable WHERE filename=?', [path])
    res = cursor.fetchone() # returns a single tuple
    cursor.close()
    if res is not None:
        return res[0]


def phototable_transform_photo(con, ptid, transformations):
    """
    Transform photo with PhotoTable.id <ptid>

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ptid : int
        PhotoTable.id of photo file to transform
    transformations : str
        Shotwell transformations string 'blob'
    """
    cursor = con.cursor()
    cursor.execute('UPDATE PhotoTable SET transformations = ? WHERE id = ?',
                   [transformations, ptid])
    con.commit()
    cursor.close()
    return


def photo_id(id):
    """
    photo_id value for use as TagTable photo_id_list entry
    
    Parameters
    ----------
    id : int
        PhotoTable.id value
        
    Returns
    -------
    str
        photo_id value for use as TagTable photo_id_list entry (== thumb file name w/o extension)
    """
    # TagTable.photo_id_list is a string with a comma-separated list
    # of photo's tagged with TagTable.name. Each entry in that list
    # refers to the PhotoTable.id. The PhotoTable.id is an integer,
    # the photo_id_list entries are strings formatted like:
    #   'thumb00000000000000a3'
    #   'thumb0000000000000fb9'
    #   'thumb000000000000126a'
    # Each photo_id in the TagTable.photo_id_list is a string 
    # starting with 'thumb'
    # followed by the hexadecimal representation of PhotoTable.id
    # padded with leading '0' to form a 16-character string.
    hexid = hex(id)[2:]
    return 'thumb' + f'0000000000000000{hexid}'[-16:]


def tags(con, id):
    """
    list tag names for photo with PhotoTable.id <id>
    
    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    id : int
        PhotoTable.id
    
    Returns
    -------
    list of str
        tag names for photo with PhotoTable.id <id>
    """
    taglist = []
    pid = photo_id(id)
    cursor = con.cursor()
    cursor.execute('SELECT name FROM TagTable WHERE photo_id_list LIKE ?', (f'%{pid}%',))
    taglist.extend(map(lambda x: x[0], cursor.fetchall()))
    cursor.close()
    return taglist


def is_hierarchical_tag(name):
    # Shotwell hierarchical tag names start with a '/'
    return re.match('^/.*', name) is not None


def is_flat_tag(name):
    return not is_hierarchical_tag(name)


def is_hierarchical_root_tag(name):
    # Shotwell hierarchical root tag names start with a '/'
    # but has no second '/'
    return (is_hierarchical_tag(name) and re.match('^/[^/]*$', name))


def tagtable_tags(con, path):
    """
    list tag names for photo file <path>

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    path : str
        path to photo file

    Returns
    -------
    list of str
    """
    ptid = phototable_id(con, path)
    if ptid is not None:
        return tags(con, ptid)


def tagtable_id(con, tagname):
    """
    Find shotwell TagTable.id for <tagname>.

    Parameters
    ----------
    con : sqlite3.Connection
        open Shotwell DB connection
    tagname : str
        TagTable.name

    Returns
    -------
    int
        TagTable.id if <tagname> exists in shotwell; None otherwise
    """
    cursor = con.cursor()
    cursor.execute('SELECT id FROM TagTable WHERE name=?', [tagname])
    res = cursor.fetchall() # returns tuple of tuples
    cursor.close()
    if len(res) > 0:
        return res[0][0] # at most one id for tagname
    else:
        return None


def tagtable_insert(con, tag):
    """
    Insert tag into TagTable.

    Parameters
    ----------
    con : sqlite3.Connection
        Open Shotwell db connection
    tag : str
        TagTable.name

    Returns
    -------
    int
        tagid
    """
    # Schema: CREATE TABLE TagTable (id INTEGER PRIMARY KEY, name TEXT UNIQUE NOT NULL, photo_id_list TEXT, time_created INTEGER);
    cursor = con.cursor()
    cursor.execute('''INSERT INTO TagTable(name,photo_id_list,time_created)
                      VALUES(?,?,?)''', [tag,'',math.floor(time.time())])
    con.commit()
    tagid = cursor.lastrowid
    cursor.close()
    return tagid


def tagtable_promote(con, ttid):
    """
    Transform from flat tag to hierarchical root tag

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ttid : int
        TagTable.id of tag for photo file
    """
    cursor = con.cursor()
    cursor.execute('SELECT name from TagTable WHERE id=?', [ttid])
    res = cursor.fetchall()
    if len(res) > 0:
        name = res[0][0]
        if is_flat_tag(name):
            name = f'/{name}'
            cursor.execute('UPDATE TagTable SET name=? WHERE id=?', [name, ttid])
    cursor.close()


def tagtable_tag_photo(con, ttid, ptid):
    """
    Add photo with PhotoTable.id <ptid> to tag with TagTable.id <ttid>.

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ttid : int
        TagTable.id of tag for photo file
    ptid : int
        PhotoTable.id of photo file to tag

    Raises
    ------
    Exception
        invalid ttid used
    """
    cursor = con.cursor()
    # find photo's tagged with tag <ttid>
    # if <ptid> not yet tagged with tag <ttid>:
    #    add ptid to <ttid>.photo_id_list
    ttpid = photo_id(ptid) # TagTable.photo_id_list entry for PhotoTable.id

    cursor = con.cursor()
    cursor.execute('SELECT photo_id_list FROM TagTable WHERE id=?', [ttid])
    rslt = cursor.fetchall()
    if len(rslt) == 1:
        photo_id_list = rslt[0][0] # photo_id_list from TagTable
        if photo_id_list is None:
            photo_id_list = ''
        if re.match(f'.*{ttpid}.*', photo_id_list) is None: # photo_id not yet tagged
            # observation: TagTable.photo_id_list always ends with a comma
            # simply extend TagTable.photo_id_list with ttpid
            photo_id_list += f'{ttpid},'
            cursor.execute('UPDATE TagTable SET photo_id_list = ? WHERE id = ?', [photo_id_list, ttid])
            con.commit()
        #else:
            # noop: photo already tagged
    else:
        raise Exception(f'Invalid TagTable.id {ttid}')
    cursor.close()


def tagtable_untag_photo(con, ttid, ptid):
    """
    Remove photo with PhotoTable.id <ptid> from tag with TagTable.id <ttid>.

    Parameters
    ----------
    con :
        open connection to Shotwell sqlite db
    ttid : int
        TagTable.id of tag
    ptid : int
        PhotoTable.id of photo file

    Raises
    ------
    Exception
        invalid ttid used
    """
    cursor = con.cursor()
    # find photo's tagged with tag <ttid>
    # if <ptid> tagged with tag <ttid>:
    #    remove ptid from <ttid>.photo_id_list
    ttpid = photo_id(ptid) # TagTable.photo_id_list entry for PhotoTable.id

    cursor = con.cursor()
    cursor.execute('SELECT photo_id_list FROM TagTable WHERE id=?', [ttid])
    rslt = cursor.fetchall()
    if len(rslt) == 1:
        cur_photo_id_list = rslt[0][0] # photo_id_list from TagTable
        # remove entry for ttpid from TagTable.photo_id_list
        new_photo_id_list = re.sub(f'{ttpid},', '', cur_photo_id_list)
        if cur_photo_id_list != new_photo_id_list:
            if new_photo_id_list == '':
                cursor.execute('DELETE FROM TagTable WHERE id = ?', [ttid])
            else:
                cursor.execute('UPDATE TagTable SET photo_id_list = ? WHERE id = ?',
                               [new_photo_id_list, ttid])
            con.commit()

    else:
        raise Exception(f'Invalid TagTable.id {ttid}')
    cursor.close()


