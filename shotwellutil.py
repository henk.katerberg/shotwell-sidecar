# shotwell-sidecar -- Shotwell metadata export/import tools

# Copyright (C) 2023  Henk Katerberg <hank@mudball.nl>

# This file is part of shotwell-sidecar

# shotwell-sidecar is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# shotwell-sidecar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

"""
shared code for shotwell-util tools
"""

import argparse
import os
import shotwelldb as db


def parse(kind, direction):
    """
    shotwell-util command-line option parsing common to import/export tools

    Parameters
    ----------
    kind : str
        Kind of data to handle: ['tags'|'transformations']
    direction : str
        ['export'|'import']
    """
    dir = 'to' if direction=='export' else 'from'
    filekind = 'sidecar' if dir=='from' else 'photo'
    desc = f'{direction} Shotwell {kind} {dir} sidecar file'

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('-d','--datadir',
                        default=db.default_datadir,
                        help=f'shotwell data directory (defaults to {db.default_datadir})')
    parser.add_argument('-f','--force',
                        help='override shotwell database version check',
                        action='store_true')
    parser.add_argument(filekind, help=f'path to {filekind} file')
    args = parser.parse_args()
    args.database = db.dbfile(args.datadir)
    args.thumbsdir = db.thumbsdir(args.datadir)
    args.datadir = os.path.abspath(os.path.expanduser(args.datadir))
    if filekind=='sidecar':
        args.sidecar = os.path.abspath(args.sidecar)
    else:
        args.photo = os.path.abspath(args.photo)

    return args
